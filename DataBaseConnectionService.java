package myserver.example;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.basex.util.Base64;

public final class DataBaseConnectionService {
	
	public static String executeQuery(String query) {
		StringBuffer response;
		try {
			// The java URL connection to the server of Thierry.
			URL url = new URL("http://localhost:8080/BaseX821/rest/Flugzeuge");
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("POST");
			
			// User and password.
			String user = "admin";
			String pw = "admin";
			
			String encoded = Base64.encode(user + ":" + pw);
			conn.setRequestProperty("Authorization", "Basic " + encoded);
			String q = "<query xmlns=\"http://basex.org/rest\"> " + "<text><![CDATA[ " + query + " ]]></text>	" + "	  </query>";
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Content-Length", "" + Integer.toString(query.getBytes().length));
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			// Send request
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(q);
			wr.flush();
			wr.close();
			
			// get response
			InputStream is = conn.getInputStream();
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, "UTF8"));
			String line;
			response = new StringBuffer();
			while ((line = rd.readLine()) != null) {
				response.append(line);
				response.append('\n');
			}
			rd.close();
			conn.disconnect();
			return response.toString();
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	// debugging.... can be deleted
	public static void main(String[] args) throws Exception {
		// String XPATHQUERY = "(//plane/name)[position() <= 5]";
		
		String XPATHQUERY = "(for $p in /planes/plane return (fn:string($p/name), fn:string($p/stueck), fn:string($p/erstflug), fn:string($p/beschreibung), fn:string($p/bild), fn:string($p/herstellerName), fn:string($p/herstellerBeschreibung)))[position() <= 28]";
		String response = executeQuery(XPATHQUERY);
		System.out.println(response);
		
	}
}