import java.io.*;

import com.hp.hpl.jena.query.*;
//import com.hp.hpl.jena.rdf.model.*;

public class Sparql{

	public static void main(String[] args) {
		
		String service = "http://de.dbpedia.org/sparql";
		String query ="PREFIX dbpedia-owl: <http://dbpedia.org/ontology/>\n"
				+ "PREFIX prop-de: <http://de.dbpedia.org/property/>\n"
				+ "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n"
				+ "PREFIX foaf: <http://xmlns.com/foaf/0.1/>\n"
				+ "SELECT DISTINCT (STR(?name) AS ?strname) ?stueck ?erstflug "
				+ "?Flugzeug ?abstract ?thumbnail ?Hersteller ?NHersteller\n"
				+ "WHERE { "
				+ "?Flugzeug a dbpedia-owl:Aircraft.\n"
				+ "?Flugzeug foaf:name ?name.\n"
				+ "?Flugzeug prop-de:st�ckzahl ?stueck.\n"
				+ "FILTER(?stueck<= \"9999999\"^^xsd:int).\n"
				+ "?Flugzeug dbpedia-owl:abstract ?abstract.\n"
				+ "OPTIONAL {?Flugzeug prop-de:erstflug ?erstflug.\n"
				+ "FILTER(?erstflug < \"2050-12-30\"^^xsd:date).}\n"
				+ "?Flugzeug dbpedia-owl:thumbnail ?thumbnail.\n"
				+ "?Flugzeug dbpedia-owl:manufacturer ?Hersteller .\n"
				+ "?Hersteller foaf:name ?NHersteller.\n"
				+ "}";
		QueryExecution e = QueryExecutionFactory.sparqlService(service, query);
		ResultSet results = e.execSelect();
		
		
		/*while (results.hasNext()){
			QuerySolution s = results.nextSolution();
			//System.out.println(s.toString());
			Literal zahl = s.getLiteral("stueck");
			RDFNode link = s.get("Flugzeug");
			if (zahl.getValue() instanceof Integer) {
				System.out.print("Flugzeug: "+link.toString()+" ");
				System.out.println("Anzahl: "+zahl.getValue());
			}
		}
		results = e.execSelect();*/
		String xmlStr = ResultSetFormatter.asXMLString(results);
		
			Writer writer = null;

			try {
				if (args.length > 0){
					 writer = new BufferedWriter(new OutputStreamWriter(
					          new FileOutputStream(args[0]), "utf-8"));
				} else {
					 writer = new BufferedWriter(new OutputStreamWriter(
					          new FileOutputStream("aircraft.xml"), "utf-8"));
				}
			   
			    writer.write(xmlStr);
			    
			} catch (IOException ex) {
			  System.out.println(ex.getStackTrace());
			} finally {
			   try {writer.close();} catch (Exception ex) {/*ignore*/}
			}
		//System.out.println(xmlStr);
		e.close();
		
		
		
	}

	
}