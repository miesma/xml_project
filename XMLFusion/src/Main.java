

import java.io.*;
import java.net.*;

public class Main {
	public static String xmlPath="aircraft.xml";//D:/work/XML/aircraft.xml
	public static String resultPath="aircraft_manufacturer.xml";//D:/work/XML/aircraft_manufacturer.xml
	public static String requestUrl="https://de.wikipedia.org/w/api.php?action=opensearch&format=xml&search=";
	//https://de.wikipedia.org/w/api.php?action=opensearch&format=xml&search=Adam%20Aircraft%20Industries
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException
	{
		if(args.length>0)
			xmlPath=args[0];
		else{
			System.out.println("Error: XML file path needed as first argument.");
			return;
		}
		
		if(args.length>1)
			resultPath=args[1];
		
		System.out.println("input xml file: "+xmlPath);
		System.out.println();
	    String prefix="<uri>http://de.dbpedia.org/resource/";
	    
		//find <uri> url </uri>
		boolean nextH=false;
		int nullIsAppend=-1;
		int counter=0;
		String toAppend=new String();
	    BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(xmlPath), "UTF8"));
	    try 
	    {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	
	        while (line != null) 
	        {
	        	nullIsAppend--;
	        	if(nullIsAppend==0)
	        	{
	        		nullIsAppend=-1;
	        		sb.append(toAppend);
		            sb.append(System.lineSeparator());
	        	}
	        	if(counter!=1)//just dont save the second line 
	        	{
		            sb.append(line);
		            sb.append(System.lineSeparator());
	        	}
	        	else//replace <sparql xmlns="http://www.w3.org/2005/sparql-results#"> with <sparql>
	        	{
		            sb.append("<sparql>");
		            sb.append(System.lineSeparator());
	        	}
	            line = br.readLine();
	            //this line is what we want
	            if(nextH)
	            {
	            	nextH=false;
	            	if(line.contains("<uri>"))
	            	{
	            		//get hersteller name
	            		String tempS=line;
	            		tempS=tempS.substring(tempS.indexOf(prefix)+prefix.length(),tempS.indexOf("</uri>"));
	            		
	            		//http request for hersteller information
	            		toAppend=xmlFromRequest(requestUrl+tempS);
	            		
	            		//just keep <SearchSuggestion ....</SearchSuggestion>
	            		String suffix="</SearchSuggestion>";
	            		/*System.out.println(tempS);
	            		System.out.print(toAppend.indexOf("<SearchSuggestion"));
	            		System.out.print(" : ");
	            		System.out.println(toAppend.indexOf(suffix)+suffix.length());
	            		System.out.println("toAppend:"+toAppend);*/
	            		toAppend=toAppend.substring(toAppend.indexOf("<SearchSuggestion"),toAppend.indexOf(suffix)+suffix.length());

	            		//remove <SearchSuggestion>
	            		toAppend=toAppend.replace("<SearchSuggestion version=\"2.0\" xmlns=\"http://opensearch.org/searchsuggest2\">\n","");
	            		toAppend=toAppend.replace("\n\n\n\n</SearchSuggestion>","");
	            		toAppend=toAppend.replace("</SearchSuggestion>","");

	            		//only take first result (if there are multiple)
	            		if(toAppend.indexOf("</Description>")>10)
	            			toAppend=toAppend.substring(0,toAppend.indexOf("</Description>")+"</Description>".length());
	            			//result=result.substring(0,result.length());
	            		
	            		//herstellers.add(tempS);
	            		
	            		//System.out.println(toAppend);
	            		//System.out.println();
	            		
	            		nullIsAppend=3;
	            	}
	            }
	            //next line contains our wanted url
	            if(line!=null && line.contains("\"Hersteller\""))
	            	nextH=true;
	        	counter++;
	        }
		    //System.out.println(sb.toString());

		    //save
		    PrintWriter writer = new PrintWriter(resultPath, "UTF-8");
		    writer.println(sb.toString());
		    writer.close();

		    System.out.println("Output written in "+resultPath+" !");
	    } 
	    finally 
	    {
	        br.close();
	    }
		
	    
	}
	
	
	//return a string with content of an http request
	public static String xmlFromRequest(String urls) throws IOException
	{
		//System.out.println("request: "+urls);

		URL url = new URL(urls);
		String result=new String();
		
		//make connection
		URLConnection urlc = url.openConnection();
		
		//use post mode
		urlc.setDoOutput(true);
		urlc.setAllowUserInteraction(false);
		
		//get result
		BufferedReader br =  new BufferedReader(new InputStreamReader(urlc.getInputStream(), "UTF8"));

		String l = null;
		while ((l=br.readLine())!=null) 
		{
		    result+=l;
			//System.out.println("line:"+l);
		}
		br.close();

		//System.out.println("result:"+result);
		//edit our results
		//end line after each thing
		result=result.replace(">",">\n");
		result=result.replace("</","\n</");
		
		//remove  xml:space="preserve"
		result=result.replace(" xml:space=\"preserve\"","");

		//remove <Item>
		result=result.replace("<Item>\n","");
		result=result.replace("</Item>\n","");
		
		//remove <Section>
		result=result.replace("<Section>\n","");
		result=result.replace("</Section>\n","");

		//rearrange end lines
		result=result.replace("<Text>\n","<Text>");
		result=result.replace("\n</Text>","</Text>");

		result=result.replace("<Description>\n","<Description>");
		result=result.replace("\n</Description>","</Description>");

		result=result.replace("<Url>\n","<Url>");
		result=result.replace("\n</Url>","</Url>");

		result=result.replace("<Query>\n","<Query>");
		result=result.replace("\n</Query>","</Query>");
		
		return result;
	}

	public static String readFile(String f) throws IOException
	{
        String everything = new String();
        BufferedReader br = new BufferedReader(new FileReader(f));
	    try 
	    {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) 
	        {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        everything = sb.toString();
	    } 
	    finally 
	    {
	        br.close();
	    }
	    return everything;
	}
}
