$(document).ready(function(){
	$('input[type=radio]').prop('checked', false);
});

selection = [0, 0, 0, 0];

function swap(number, option) {
	if(selection[number - 1] == option) {
		return;
	}
	var conflict = isSelected(option);
	if(conflict == 0) {
		selection[number - 1] = option;
		return;
	}
	if(selection[number - 1] != 0) {
		if(selection[number - 1] > option) {
			var tmp = isSelected(option);
			for(i = option; i < selection[number - 1]; i++) {
				if(tmp == 0){
					break;
				}
				var tmp2 = isSelected(i + 1);
				selection[tmp - 1]++;
				tmp = tmp2;
			}
		} else {
			var tmp = isSelected(option);
			for(i = option; i > selection[number - 1]; i--) {
				if(tmp == 0){
					break;
				}
				var tmp2 = isSelected(i - 1);
				selection[tmp - 1]--;
				tmp = tmp2;
			}
		}
	} else {
		
	}
	selection[number - 1] = option;
	var list = $('input[type=radio][name=first]');
	for(i = 4; i --> 0;) {
		$(list[i]).prop('checked', i + 1 == selection[0]);
	}
	var list = $('input[type=radio][name=second]');
	for(i = 4; i --> 0;) {
		$(list[i]).prop('checked', i + 1 == selection[1]);
	}
	var list = $('input[type=radio][name=third]');
	for(i = 4; i --> 0;) {
		$(list[i]).prop('checked', i + 1 == selection[2]);
	}
	var list = $('input[type=radio][name=fourth]');
	for(i = 4; i --> 0;) {
		$(list[i]).prop('checked', i + 1 == selection[3]);
	}
}

function isSelected(option) {
	var conflict = 0;
	for(var i = 4; i --> 0;){
		if(selection[i] == option){
			conflict = i + 1;
		}
	}
	return conflict;
}

function checkResult() {
	var correct = true;
	for(var i = 4; i --> 0;){
		if(correctList[i] != selection[i]){
			correct = false;
			$('.selector')[i].classList.remove('green');
			$('.selector')[i].classList.add('red');
		}else{
			$('.selector')[i].classList.remove('red');
			$('.selector')[i].classList.add('green');
		}
	}
	if(!correct){
		return;
	}
	var xl = $('.invis');
	for(var i = xl.length; i --> 0;){
		xl[i].classList.remove('invis');
	}
}