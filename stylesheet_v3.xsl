﻿<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >

 <xsl:template match="/">
  <planes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="localhost:8080/xml/xml_database_v3_schema.xsd">
     <xsl:apply-templates/>
  </planes>
 </xsl:template>

 <xsl:template match="/*">
   <xsl:apply-templates/>
 </xsl:template>
 
  <xsl:template match="/*">
<xsl:apply-templates/>
 </xsl:template>
 
   <xsl:template match="/*">
<xsl:apply-templates/>
 </xsl:template>
 
    <xsl:template match="/*">
<xsl:apply-templates/>
 </xsl:template>
 
  <xsl:template match="result">
<plane><xsl:apply-templates/></plane>
 </xsl:template>
 
 <xsl:template match="binding[@name='strname']">
<name><xsl:copy-of select="node()/text()"/></name>
 </xsl:template>
 
<xsl:template match="binding[@name='stueck']">
<stueck><xsl:copy-of select="node()/text()"/></stueck>
 </xsl:template>
 
<xsl:template match="binding[@name='erstflug']">
<erstflug><xsl:copy-of select="node()/text()"/></erstflug>
 </xsl:template>
 
<xsl:template match="binding[@name='Flugzeug']">
<uri><xsl:copy-of select="node()/text()"/></uri>
 </xsl:template>
 
<xsl:template match="binding[@name='abstract']">
<beschreibung><xsl:copy-of select="node()/text()"/></beschreibung>
 </xsl:template>
 
 <xsl:template match="binding[@name='thumbnail']">
<bild><xsl:copy-of select="node()/text()"/></bild>
 </xsl:template>

 <xsl:template match="binding[@name='Hersteller']">
<herstellerURI><xsl:copy-of select="node()/text()"/></herstellerURI>
 </xsl:template>
 
  <xsl:template match="URL[@xml:space='preserve']">
<herstellerURL><xsl:copy-of select="node()/text()"/></herstellerURL>
 </xsl:template>
 
  <xsl:template match="binding[@name='NHersteller']">
<herstellerName><xsl:copy-of select="node()/text()"/></herstellerName>
 </xsl:template>
 
<xsl:template match="Url">
<herstellerUrl><xsl:copy-of select="text()"/></herstellerUrl>
 </xsl:template>
 
 <xsl:template match="Description">
<herstellerBeschreibung><xsl:copy-of select="text()"/></herstellerBeschreibung>
 </xsl:template>
 
  <xsl:template match="Query">
 </xsl:template>
 
<xsl:template match="Text">
 </xsl:template>

 



</xsl:stylesheet>