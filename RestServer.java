package myserver.example;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

// Plain old Java Object it does not extend as class or implements
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation.
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML.
// http://q1k.de:8088/ /home/xml/res
// http://q1k.de:8088/xml3/xml_Database_v3.xml

// The browser requests per default the HTML MIME type.

// Sets the path to base URL + /any Element
@Path("/")
public class RestServer extends Application {
	
	// This method is called if XML is request
	@GET
	@Path("/planes")
	@Produces(MediaType.APPLICATION_XML)
	public String getName() {
		String query = "for $p in /planes\nreturn $p";
		String result = DataBaseConnectionService.executeQuery(query);
		return result;
	}
	
	@GET
	@Path("/plane/{name}")
	@Produces(MediaType.APPLICATION_XML)
	public String getStueck(@PathParam("name") String name) {
		try {
			name = URLDecoder.decode(name, "UTF-8");
		} catch (UnsupportedEncodingException e1) {}
		String query = "for $p in /planes/plane\n where $p/name = '"+name+"'\n return $p";
		String result = DataBaseConnectionService.executeQuery(query);
		return result;
	}
}