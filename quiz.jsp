<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="org.basex.query.value.item.Item"%>
<%@page import="org.basex.query.iter.Iter"%>
<%@page import="org.basex.query.QueryProcessor"%>
<%@page import="org.basex.core.Context"%>
<%@page import="org.basex.core.cmd.XQuery"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.net.ProtocolException"%>
<%@page import="java.net.URL"%>
<%@page import="org.basex.util.Base64"%>
<%
	String query = "declare namespace math = \"java:java.lang.Math\";\n"
			+ "(for $p in /planes/plane\n"
			+ "order by math:random()\n"
			+ "return (fn:string($p/name), fn:string($p/stueck), fn:string($p/erstflug), fn:string($p/beschreibung), fn:string($p/bild), fn:string($p/herstellerName), fn:string($p/herstellerBeschreibung)))[position() <= 28]";
	String[] name = new String[4];
	int[] stueck = new int[4];
	Date[] dates = new Date[4];
	Date never = new Date();
	String[] abstracts = new String[4];
	String[] imgUrls = new String[4];
	String[] manufactor = new String[4];
	String[] mfabstract = new String[4];
	SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd'Z'");
	SimpleDateFormat sdf2 = new SimpleDateFormat("YYYY");
	
	BufferedReader br = null;
	try {
		// The java URL connection to the server of Thierry.
		URL url = new URL("http://localhost:8080/BaseX821/rest/Flugzeuge");
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		
		conn.setRequestMethod("POST");
		
		// User and password.
		String user = "admin";
		String pw = "admin";
		
		String encoded = Base64.encode(user + ":" + pw);
		conn.setRequestProperty("Authorization", "Basic " + encoded);
		String q = "<query xmlns=\"http://basex.org/rest\"> " + "<text><![CDATA[ " + query + " ]]></text>	" + "	  </query>";
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		conn.setRequestProperty("Content-Length", "" + Integer.toString(query.getBytes().length));
		conn.setRequestProperty("Content-Language", "en-US");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		
		// Send request
		DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
		wr.writeBytes(q);
		wr.flush();
		wr.close();
		
		// get response
		InputStream is = conn.getInputStream();
		br = new BufferedReader(new InputStreamReader(is, "UTF8"));		
	} catch (MalformedURLException e) {
		//e.printStackTrace();
	} catch (ProtocolException e) {
		//e.printStackTrace();
	} catch (IOException e) {
		//e.printStackTrace();
	}
	
	for (int i = 0; i < 4; i++) {
		name[i] = br.readLine();
		stueck[i] = Integer.parseInt(br.readLine());
		String date = br.readLine();
		if (date.length() == 4) {
			dates[i] = sdf2.parse(date);
		} else if (date.length() == 11) {
			dates[i] = sdf.parse(date);
		} else {
			dates[i] = never;
		}
		abstracts[i] = br.readLine();
		imgUrls[i] = br.readLine();
		manufactor[i] = br.readLine();
		mfabstract[i] = br.readLine();
	}
	br.close();
	boolean search = Math.random() > 0.5;
	int[] correct = new int[4];
	for (int i = 0; i < 4; i++) {
		correct[i] = i + 1;
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if(i == j){
				continue;
			}
			if (search) {
				if (dates[j].after(dates[i]) == correct[j] < correct[i]) {
					int tmp = correct[j];
					correct[j] = correct[i];
					correct[i] = tmp;
				}
			} else {
				if (stueck[j] < stueck[i] == correct[j] < correct[i]) {
					int tmp = correct[j];
					correct[j] = correct[i];
					correct[i] = tmp;
				}
			}
		}
	}
	String searchFor = search ? "Erstflugsdatum" : "Stückzahl" ;
%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:foaf="http://xmlns.com/foaf/0.1/">
<head>
<title>XML Projekt: Flugzeug-Quiz</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="quiz.css" />
<script src="jquery-2.1.4.min.js"></script>
<script>
	correctList = [
<%=correct[0]%>
	,
<%=correct[1]%>
	,
<%=correct[2]%>
	,
<%=correct[3]%>
	];
</script>
<script src="quiz.js"></script>
</head>
<body>
	Sortiere nach: <%=searchFor%>
	<form>
		<table border=1>
			<tr>
				<td class="trtext invis"><%=name[0]%><br /><%=abstracts[0]%><br /><br />Erstflug: <%=dates[0]%><br />Stueckzahl: <%=stueck[0]%><br /><br />Hersteller: <%=manufactor[0]%><br /><%=mfabstract[0]%></td>
				<td><img src="<%=imgUrls[0]%>" /></td>
				<td><img src="<%=imgUrls[1]%>" /></td>
				<td class="trtext invis"><%=name[1]%><br /><%=abstracts[1]%><br />Erstflug: <%=dates[1]%><br />Stueckzahl: <%=stueck[1]%><br /><br />Hersteller: <%=manufactor[1]%><br /><%=mfabstract[1]%></td>
			</tr>
			<tr>
				<td class="invis"></td>
				<td class="selector"><input type="radio" name="first" value="1"
					onclick="swap(1, 1)" /> <input type="radio" name="first" value="2"
					onclick="swap(1, 2)" /> <input type="radio" name="first" value="3"
					onclick="swap(1, 3)" /> <input type="radio" name="first" value="4"
					onclick="swap(1, 4)" /></td>
				<td class="selector"><input type="radio" name="second"
					value="1" onclick="swap(2, 1)" /> <input type="radio"
					name="second" value="2" onclick="swap(2, 2)" /> <input
					type="radio" name="second" value="3" onclick="swap(2, 3)" /> <input
					type="radio" name="second" value="4" onclick="swap(2, 4)" /></td>
				<td class="invis"></td>
			</tr>
			<tr>
				<td class="invis"></td>
				<td class="selector"><input type="radio" name="third" value="1"
					onclick="swap(3, 1)" /> <input type="radio" name="third" value="2"
					onclick="swap(3, 2)" /> <input type="radio" name="third" value="3"
					onclick="swap(3, 3)" /> <input type="radio" name="third" value="4"
					onclick="swap(3, 4)" /></td>
				<td class="selector"><input type="radio" name="fourth"
					value="1" onclick="swap(4, 1)" /> <input type="radio"
					name="fourth" value="2" onclick="swap(4, 2)" /> <input
					type="radio" name="fourth" value="3" onclick="swap(4, 3)" /> <input
					type="radio" name="fourth" value="4" onclick="swap(4, 4)" /></td>
				<td class="invis"></td>
			</tr>
			<tr>
				<td class="trtext invis"><%=name[2]%><br /><%=abstracts[2]%><br />Erstflug: <%=dates[2]%><br />Stueckzahl: <%=stueck[2]%><br /><br />Hersteller: <%=manufactor[2]%><br /><%=mfabstract[2]%></td>
				<td><img src="<%=imgUrls[2]%>" /></td>
				<td><img src="<%=imgUrls[3]%>" /></td>
				<td class="trtext invis"><%=name[3]%><br /><%=abstracts[3]%><br />Erstflug: <%=dates[3]%><br />Stueckzahl: <%=stueck[3]%><br /><br />Hersteller: <%=manufactor[3]%><br /><%=mfabstract[3]%></td>
			</tr>
		</table>
		<input type="button" value="Bewerten" onclick="checkResult()" />
	</form>
	Made by
	<span property="dc:creator">
		<span typeof="foaf:Person" property="foaf:name">Timo Gielser</span>,
		<span typeof="foaf:Person" property="foaf:name">Markus Mies</span>,
		<span typeof="foaf:Person" property="foaf:name">Julian Petrasch</span>,
		<span typeof="foaf:Person" property="foaf:name">Thiery Meurers</span>,
		<span typeof="foaf:Person" property="foaf:name">Alexis Iakovenko</span> and 
		<span typeof="foaf:Person" property="foaf:name">Shiho Onitsuka</span>
	</span>
</body>
</html>