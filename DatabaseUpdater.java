package updater;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.basex.util.Base64;

public final class DatabaseUpdater {
	
	public static boolean delete(String filename) {
		try {
			URL url = new URL("http://localhost:8080/BaseX821/rest/Flugzeuge/" + filename);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("DELETE");
			
			String user = "admin";
			String pw = "admin";
			
			String encoded = Base64.encode(user + ":" + pw);
			conn.setRequestProperty("Authorization", "Basic " + encoded);
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			InputStream is = null;
			try {
				is = conn.getInputStream();
			} catch (IOException ioe) {
				is = conn.getErrorStream();
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, "UTF8"));
			while (rd.readLine() != null) {
			}
			rd.close();
			conn.disconnect();
			return conn.getResponseCode() == 200;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean put(String filename) {
		try {
			URL url = new URL("http://localhost:8080/BaseX821/rest/Flugzeuge/" + filename);
			
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestMethod("PUT");
			
			String user = "admin";
			String pw = "admin";
			File f = new File(filename);
			String encoded = Base64.encode(user + ":" + pw);
			conn.setRequestProperty("Authorization", "Basic " + encoded);
			conn.setRequestProperty("Content-Type", "application/xml");
			conn.setRequestProperty("Content-Length", "" + Long.toString(f.length()));
			conn.setRequestProperty("Content-Language", "en-US");
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			FileInputStream fis = new FileInputStream(f);
			OutputStream os = conn.getOutputStream();
			byte[] buffer = new byte[256];
			int bytesRead = 0;
			while ((bytesRead = fis.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			fis.close();
			os.flush();
			os.close();
			
			InputStream is = null;
			try {
				is = conn.getInputStream();
			} catch (IOException ioe) {
				is = conn.getErrorStream();
			}
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, "UTF8"));
			while (rd.readLine() != null) {
			}
			rd.close();
			conn.disconnect();
			return conn.getResponseCode() == 201;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			throw new IllegalArgumentException("Invalid Parameters!");
		}
		// xml_Database_v3.xml
		delete(args[0]);
		Thread.sleep(2000);
		put(args[0]);
	}
}